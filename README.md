# hpAMReX

## Description
**hpAMReX** is a collection of software libraries for applications that require **hp** **A**daptive **M**esh **R**efinement algorithms, may run on massively parallel architectures and target **eX**ascale computing. **hpAMReX** is based on the software framework [AMReX](https://amrex-codes.github.io/amrex/docs_html/)

## Graphical Abstract
![alt text](./Ad/GraphicalAbstract.jpeg)

## Installation
Routines may be provided upon request contacting [Vincenzo Gulizzi](mailto:vincenzo.gulizzi@unipa.it).

## Authors and acknowledgment
Main developers: [Vincenzo Gulizzi](mailto:vincenzo.gulizzi@unipa.it)

We acknowledge Weiqun Zhang and Robert Saye for their support in the use of [AMReX](https://amrex-codes.github.io/amrex/docs_html/) and [Algoim](https://algoim.github.io), respectively.

## License
TBA.

## Project status
In progress.

## Applications
Below the list of Journal and Conference papers using hpAMReX.
### Journal papers
* Mattei, O., & Gulizzi, V. (2023). On the effects of suitably designed space microstructures in the propagation of waves in time modulated composites. _Applied Physics Letters_, 122(6). [doi](https://doi.org/10.1063/5.0132899)
* Gulizzi, V., & Saye, R. (2022). Modeling wave propagation in elastic solids via high-order accurate implicit-mesh discontinuous Galerkin methods. _Computer Methods in Applied Mechanics and Engineering_, 395, 114971. [doi](https://doi.org/10.1016/j.cma.2022.114971)
* Gulizzi, V., Almgren, A. S., & Bell, J. B. (2022). A coupled discontinuous Galerkin-Finite Volume framework for solving gas dynamics over embedded geometries. _Journal of Computational Physics_, 450, 110861. [doi](https://doi.org/10.1016/j.jcp.2021.110861)
### Conference papers
* Gulizzi, V. (2023). A high-order accurate framework for thermal fluid-structure interaction. In _Aerospace Europe Conference 2023 – 10TH EUCASS – 9TH CEAS_, Lausanne, Switzerland. [link](https://www.eucass.eu/component/docindexer/?task=download&id=6723)
* Gulizzi, V. (2022). HIGH-ORDER ACCURATE EMBEDDED-BOUNDARY DISCONTINUOUS GALERKIN METHODS FOR INVISCID GAS DYNAMICS. In _ICAS PROCEEDINGS 33th Congress of the International Council of the Aeronautical Sciences_, Stockholm, Sweden. [link](https://www.icas.org/ICAS_ARCHIVE/ICAS2022/data/papers/ICAS2022_0735_paper.pdf)